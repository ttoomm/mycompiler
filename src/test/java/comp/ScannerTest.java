package comp;

import comp.token.Token;
import comp.token.TokenType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScannerTest {

    final static Logger log= LoggerFactory.getLogger(ScannerTest.class);

    Scanner sc;

    @Test
    public void tryRegexMatchIsSymbol() throws Exception {
        this.sc = new Scanner(new String[]{"a b c ", "4343 332.21e+2", "21{"});
        TokenType[] expectedTts=new TokenType[]{
                TokenType.tId, TokenType.tId, TokenType.tId,
                TokenType.tInteger, TokenType.tFloat,
                TokenType.tInteger,TokenType.tPunctuation
        };
        String[] vals=new String[]{"a", "b", "c", "4343", "332.21e+2", "21", "{"};
        for(int i=0; i<vals.length; i++) {
            String v=vals[i];
            Token t = this.sc.nextToken();
            Assert.assertEquals(expectedTts[i], t.tp);
            Assert.assertEquals(v, t.value);
        }

    }

    @Test
    public void tryRegexMatchIsSymbolWithCommentSingleLine() throws Exception {
        this.sc = new Scanner(new String[]{"a b c // fdsafdsa", "4343 332.21e+2", "21/vv"});
        TokenType[] expectedTts=new TokenType[]{
                TokenType.tId, TokenType.tId, TokenType.tId, TokenType.tPunctuation,
                TokenType.tInteger, TokenType.tFloat,
                TokenType.tInteger,TokenType.tOperator, TokenType.tId
        };
        String[] vals=new String[]{"a", "b", "c", "//", "4343", "332.21e+2", "21", "/", "vv"};
        for(int i=0; i<vals.length; i++) {
            String v=vals[i];
            Token t = this.sc.nextToken();
            if(t.value.equals("//")) sc.skipComment(true);
            Assert.assertEquals(expectedTts[i], t.tp);
            Assert.assertEquals(v, t.value);
        }
    }

    @Test
    public void tryRegexMatchIsSymbolWithCommentMultiLine() throws Exception {
        this.sc = new Scanner(new String[]{
                "a b /* fdsafdsa */c",
                "4343 332.21e+2/*",                "*/",
                "21/vv"
        });
        TokenType[] expectedTts=new TokenType[]{
                TokenType.tId, TokenType.tId, TokenType.tPunctuation,TokenType.tId,
                TokenType.tInteger, TokenType.tFloat,TokenType.tPunctuation,
                TokenType.tInteger,TokenType.tOperator, TokenType.tId
        };
        String[] vals=new String[]{
                "a", "b", "/*", "c",
                "4343", "332.21e+2", "/*",
                "21", "/", "vv"};
        for(int i=0; i<vals.length; i++) {
            log.info("{}- expected: {}, {}",  i, expectedTts[i], vals[i]);
            String v=vals[i];
            Token t = this.sc.nextToken();
            if(t.value.equals("/*")){ sc.skipComment(false); }
            Assert.assertEquals(expectedTts[i], t.tp);
            Assert.assertEquals(v, t.value);
        }
    }



}