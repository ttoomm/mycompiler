package comp;

import comp.token.Token;
import comp.token.TokenType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScannerSimpleTest {

    Scanner sc;

    @Before
    public void setup(){
        this.sc = new Scanner(new String[]{});
    }

    @Test
    public void tryRegexMatchIsSymbol() throws Exception {
        String expectSymbol="{";
        for(String line: new String[]{"{fds", "{322332", "{//}nfdsafd"}) {
            Token t = sc.tryGetSymbol(0, line);
            Assert.assertEquals(TokenType.tPunctuation, t.tp);
            Assert.assertEquals(expectSymbol, t.value);
        }
    }

    @Test
    public void tryRegexMatchIsKeywork() {
        String kw="main";
        Token t = sc.tryGetNonSymbol(0, kw);
        Assert.assertEquals(TokenType.tKeyword, t.tp);
        Assert.assertEquals(kw, t.value);

    }

    @Test
    public void tryRegexMatchIsIdentifier() {
        String kw="abcd";

        Token t = sc.tryGetNonSymbol(0, kw);
        Assert.assertEquals(TokenType.tId, t.tp);
        Assert.assertEquals(kw, t.value);

    }

    @Test
    public void tryRegexMatchIsInt() {
        String kw="332432";

        Token t = sc.tryGetNonSymbol(0, kw);
        Assert.assertEquals(TokenType.tInteger, t.tp);
        Assert.assertEquals(kw, t.value);

    }

    @Test
    public void tryRegexMatchIsFloat() {
        for(String kw  :  new String[]{
                "332.432", "0.3",
                "3.2e2"}){
        Token t = sc.tryGetNonSymbol(0, kw);
        Assert.assertEquals(TokenType.tFloat, t.tp);
        Assert.assertEquals(kw, t.value);
        }
    }




    @Test
    public void scratch() {
        String ss="332.432e3";
        String re=TokenType.tFloat.re;

        re=String.format("^%s$", re);
        re="^(0|([1-9]\\d*))\\.((\\d*[1-9])|0)(e[\\+\\-]?(0|([1-9]\\d*)))?$";
        //re="^332\\.(\\d*[1-9])|0](e[\\+\\-]?(0|([1-9]\\d*)))?$";
        //re="^332\\.432(e[\\+\\-]?(0|([1-9]\\d*)))?$";
        Pattern p=Pattern.compile(re);
        Matcher m = p.matcher(ss);
        boolean b=m.matches();
        String g=m.group(0);


        String v="aaaaddfd";
        String regex="^((aaaa|bbb|cc)).+$";

        Assert.assertTrue(v.matches(regex));
        Assert.assertTrue("bbb1221".matches(regex));
        Assert.assertTrue("ccc4334".matches(regex));

        p=Pattern.compile(regex);
        m = p.matcher(v);
        Assert.assertTrue(m.matches());

        int i=m.groupCount();

        Assert.assertEquals("aaaa", m.group(1));

    }
}