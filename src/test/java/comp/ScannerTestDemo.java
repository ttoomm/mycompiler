package comp;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;

public class ScannerTestDemo {

    final static Logger log= LoggerFactory.getLogger(ScannerTestDemo.class);
    Scanner sc;
    String path, destPath="/var/tmp/scanner.test.tk.txt";
    @Before public void setup() throws IOException {
        URL testSrcFile = this.getClass().getClassLoader().getResource("testSrcFile.txt");
        path = testSrcFile.getPath();
        this.sc=new Scanner(path);
    }

    @Test
    public void testWriteTokenFile() throws IOException {

        comp.Main.writeTokenFile(this.path, this.destPath);

    }



}