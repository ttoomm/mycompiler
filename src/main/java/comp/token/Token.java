package comp.token;

public class Token {
    public TokenType tp;
    public String value;
    public int posLine, idxLine;

    public Token(TokenType tp, String value, int posLine, int idxLine) {
        this.tp = tp;
        this.value = value;
        this.posLine = posLine;
        this.idxLine = idxLine;
    }
}
