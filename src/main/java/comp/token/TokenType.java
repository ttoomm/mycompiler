package comp.token;

import comp.Scanner;

import java.util.regex.Pattern;

public enum TokenType {

    tId("[a-zA-Z][a-zA-Z0-9_]*"),
    tInteger(TokenType.ReInt),
    tFloat(String.format("(%s)\\.((\\d*[1-9])|0)(e[\\+\\-]?(%s))?", TokenType.ReInt, TokenType.ReInt)), //Todo,
    tOperator(null),
    tPunctuation(null),
    tKeyword(Keyword.regex),
    tError(null);

    static final String ReInt="0|([1-9]\\d*)";
    public final Pattern p;
    public final String re;

    TokenType(String re){
        this.re=re;
        this.p=re==null? null:Pattern.compile(re);
    }
}
