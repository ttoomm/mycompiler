package comp.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

/*
 * include Operators and punctuation
 */
public enum Symbol {

    symEq("==", true),
    symNeq("<>", true),
    symLess("<", true),
    symGt(">", true),
    symLessOrEq("<=", true),
    symGtOrEq(">=", true),
    symSemiColon(";", false),
    symComma(",", false),
    symStop(".", true),
    symColon(":", false),
    symDoubleColon("::", false),
    symPlus("+", true),
    symMinus("-", true),
    symMulti("*", true),
    symDiv("/", true),
    symAssign("=", true),
    symAnd("&&", true),
    symNot("!", true),
    symOr("||", true),
    symLeftParenthese("(", false),
    symRightParenthese(")", false),
    symLeftCurlyBracket("{", false),
    symRightCurlyBracket("}", false),
    symLeftSquareBracket("[", false),
    symRightSquareBracket("]", false),
    symCommentStart("/*", false),
    symCommentEnd("*/", false),
    symLineComment("//", false);

    final static Logger log= LoggerFactory.getLogger(Symbol.class);
    public final static Set<String> vals;
    public final static int maxLen, minLen;
    static {
        vals = Arrays.stream(Symbol.values()).map(p->p.val).collect(Collectors.toSet());
        maxLen= Collections.max(vals.stream().map(p->p.length()).collect(Collectors.toList()));
        minLen=Collections.min(vals.stream().map(p->p.length()).collect(Collectors.toList()));
    }

    public String val;
    public boolean isOperator;
    Symbol(String val, boolean isOperator){
        this.val=val;
        this.isOperator=isOperator;
    }

    static public Symbol getFromString(String s) throws Exception {
        for(Symbol sym: Symbol.values()){
            if(sym.val.equals(s)) return sym;
        }
        String msg=String.format("%s can't get the value from string: %s", Symbol.class.getName(), s);
        log.error(msg);
        throw new Exception(msg);
    }
}
