package comp.token;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

public enum Keyword {
    kwIf("if"),
    kwThen("then"),
    kwElse("else"),
    kwFor("for"),
    kwClass("class"),
    kwInteger("integer"),
    kwFloat("float"),
    kwRead("read"),
    kwWrite("write"),
    kwReturn("return"),
    kwMain("main");

    public final static Set<String> vals;
    public final static int maxLen,minLen;
    public final static String regex;

    static {
        vals = Arrays.stream(Keyword.values()).map(p->p.val).collect(Collectors.toSet());
        maxLen= Collections.max(vals.stream().map(p->p.length()).collect(Collectors.toList()));
        minLen=Collections.min(vals.stream().map(p->p.length()).collect(Collectors.toList()));
        regex=String.format("%s", String.join("|", Keyword.vals));
    }

    public String val;
    Keyword(String val){
        this.val=val;
    }


}
