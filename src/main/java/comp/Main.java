package comp;

import comp.token.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    final private static Logger log= LoggerFactory.getLogger(Main.class);

    static public void main(String[] argv) throws Exception {

        //String testpath="/home/mm/IdeaProjects/mycompiler/src/main/resources/testSourceFile.txt";
        String srcFile=Main.class.getClassLoader().getResource("testSourceFile.txt").getPath();
        //String srcFile=Main.class.getClassLoader().getResource("testSrcFile.txt").getPath();

        if(argv.length==1 ){
            srcFile = argv[0];

        }
        log.info("use the sample file: {}, \noutput will be in the same directory as the src file with postfix .tk.txt. ",srcFile);
        log.info("start compiler: ....", String.join(" ", argv));
        writeTokenFile(srcFile, srcFile.substring(0, srcFile.length()-4)+".tk.txt");
        log.info("compile finish");

    }

    public static void writeTokenFile(String src, String dst) throws IOException {
        Scanner scanner=new Scanner(src);
        PrintWriter pw=new PrintWriter(new FileWriter(dst));
        log.info("start token retrieving ...");

        boolean exceptionHappen=false, isBeginning=true;
        Token t=null;

        while( isBeginning||exceptionHappen||t!=null){
            isBeginning=false;
            try {
                t=scanner.nextToken();
                if(t==null) break;
                exceptionHappen=false;
                String tkinfo = String.format("[line# %d - offset: %d] tkType: %s, value: %s", t.idxLine, t.posLine, t.tp, t.value);
                log.info(tkinfo);
                pw.println(tkinfo);
                if (t.value.equals("//")) scanner.skipComment(true);
                if (t.value.equals("/*")) scanner.skipComment(false);
            } catch (Exception e) {
                e.printStackTrace();
                String tkinfo = String.format("[Line# %d - offset: %d]!!!!! Error., content: %s",
                        scanner.idxLine, scanner.posLine, scanner.lines.get(scanner.idxLine));
                pw.println(tkinfo);
                scanner.skipCurrentLine();
                exceptionHappen=true;
            }
        }
        pw.close();
    }
}
