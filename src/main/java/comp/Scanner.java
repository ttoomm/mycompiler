package comp;
import comp.token.Symbol;
import comp.token.Token;
import comp.token.TokenType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Integer.min;

public class Scanner {

    final private static Logger log= LoggerFactory.getLogger(Scanner.class);

    private boolean isBreak(char c){
        return  ((c=='\t') || (c==' '));
    }

    private boolean isSymbol(String s){
        return Symbol.vals.contains(s);
    }



    List<String> lines=new ArrayList<>();

    public Scanner(String filepath) throws IOException {
        log.info("start read from file: "+filepath);
        BufferedReader br=new BufferedReader(new FileReader(filepath));
        for(String line=br.readLine(); null!=line;line=br.readLine()){
            this.lines.add(line);
        }
        log.info("Total line read: {}", lines.size());
    }

    public Scanner(String[] lines) {
        for (int i = 0; i < lines.length; i++) {
            if (lines[i] == null ||
            lines[i].trim().length() == 0)
            continue;
            this.lines.add(lines[i]);

        }
    }

    public int posLine=0, idxLine=0;


    Token tryGetSymbol(int posLine, String line) throws Exception {
        final int maxlen = min(line.length() - posLine, Symbol.maxLen);
        for (int l = maxlen; l >= Symbol.minLen; l--) {
            String sym = line.substring(posLine, posLine + l);
            if (!this.isSymbol(sym)) continue;
            Symbol symbol = Symbol.getFromString(sym);
            TokenType tt = symbol.isOperator ? TokenType.tOperator : TokenType.tPunctuation;
            return new Token(tt, sym,posLine, idxLine);
        }
        return null;
    }

    protected String tryRegexMatch(int posLine, String line, String re){
        String regex=String.format("^(%s)[\\W].*$", re);
        String remainingLine=line.substring(posLine)+" ";
        if(!remainingLine.matches(regex)){
            return null;
        }
        Pattern p = Pattern.compile(regex);
        Matcher m=p.matcher(remainingLine);
        m.matches();
        String s= m.group(1);
        return s;
    }

    protected Token tryGetNonSymbol(int posLine, String line){
        TokenType[] tts=new TokenType[]{TokenType.tKeyword, TokenType.tId, TokenType.tFloat, TokenType.tInteger};
        for(int order=0; order<tts.length; order++){
            String s = this.tryRegexMatch(posLine, line, tts[order].re);
            if(s==null) continue;
            return new Token(tts[order], s, posLine, this.idxLine);
        }
        return null;
    }

    public void skipComment(boolean singleLineComment) throws Exception {
        String line=this.lines.get(this.idxLine);
        if(singleLineComment) {
            this.posLine = line.length();
            return;
        }
        String remaining=line.substring(this.posLine);
        int idx = remaining.indexOf(Symbol.symCommentEnd.val);
        if(idx>=0){
            posLine+=idx+Symbol.symCommentEnd.val.length();
            return;
        }

        if(++this.idxLine>=this.lines.size()){
            String msg="Could not find the commend end";
            log.error(msg);
            throw new Exception(msg);
        }
        posLine=0;
        this.skipComment(singleLineComment);
    }

    //public Token nextToken() throws Exception {
    public Token nextToken() throws Exception {
        if(this.idxLine==this.lines.size()){
            log.info("source file reading finish");
            return null;
        }
        String line=this.lines.get(this.idxLine);

        if(this.posLine>=line.length()){
            log.info("next line: {}", ++this.idxLine);
            this.posLine=0;
            return this.nextToken();
        }

        // multiple blank or tab, keep reading til non-break char
        if(this.isBreak(line.charAt(posLine))){
            posLine++;
            if(posLine>=line.length())
                return this.nextToken();
        }

        // symbols check
        // max match, check 2 char first
        Token t= this.tryGetSymbol(posLine, line);
        if(t!=null){
            posLine+=t.value.length();
            return t;
        }

        // non-symbol try
        t=this.tryGetNonSymbol(posLine, line);
        if(t!=null) {
            posLine += t.value.length();
            return t;
        }


        String message=String.format("Could not find next token, Pls check line %d, from pos: %d\nLine: %s", this.idxLine, this.posLine, line);
        log.error(message);

        throw new Exception(message);

    }

    // in case caller want to recover from error, then the call can skip the current line.
    public void skipCurrentLine(){
        this.posLine=this.lines.get(this.idxLine).length();
    }


}
